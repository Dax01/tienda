<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Gadgets design</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link rel="stylesheet" href="style.css" type="text/css" charset="utf-8" />
</head>
<body>
<div id="outer">
  <div id="wrapper">
    <div id="nav">
      <div id="nav-left">
        <div id="nav-right">
          <ul>
            <li><a href="#">NOSOTROS</a></li>
            <li><a href="#">PRODUCTOS</a></li>
            <li><a href="#">SERVICIOS</a></li>
            <li><a href="#">SHOPPING CART</a></li>
            <li><a href="#">NEW GADGETS</a></li>
            <li><a href="#">REGISTRO</a></li>
          </ul>
        </div>
      </div>
      <div class="clear"></div>
    </div>
    <div id="head">
      <div id="head-left"></div>
      <div id="head-right"></div>
      <div id="head-1"></div>
      <h1><span class="logo"><span class="top">top</span><span class="gadgets">gadgets</span></span></h1>
      <div id="navb">
        <ul>
          <li><a href="#">HOME</a></li>
          <li><a href="#">CONTACTO</a></li>
        </ul>
      </div>
    </div>
    <div id="head-2"></div>
    <div id="login">
      <div id="login-bot">
        <div id="login-box">
          <h2 class="login"><em>user</em>login</h2>
          <form action="#">
            <div id="login-username">
              <div>
                <label for="username">username</label>
                :
                <input type="text" name="username" value="" id="username" />
              </div>
              <div>
                <label for="password">password</label>
                :
                <input type="password" name="password" value="" id="password" />
              </div>
            </div>
            <div id="login-button">
              <input type="image" src="images/btn_login.gif" name="l" value="h" id="l" />
            </div>
            <div class="clear">
              <div class="reg"> Nuevo usuario? <a href="#">REGISTRO GRATIS</a> </div>
            </div>
          </form>
        </div>
        <div id="login-welcome">
          <div>
            <h2>Bienvenido</h2>
            <p>No olvide revisar <a href="#">este sitio diariamente</a> ya que agregamos articulos diariamente!.</p>
          </div>
        </div>
        <div class="clear"></div>
      </div>
    </div>